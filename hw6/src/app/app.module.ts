import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import{FormsModule} from '@angular/forms'
import {MatCardModule} from '@angular/material/card';
import {MatInputModule} from '@angular/material/input';

import { AppComponent } from './app.component';
import { SingupComponent } from './singup/singup.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { Routes, RouterModule } from '@angular/router';

import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import {environment} from '../environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    SingupComponent,
    WelcomeComponent
  ],
  imports: [
    BrowserModule,
    MatCardModule,
    FormsModule,
    BrowserAnimationsModule,
    MatInputModule,//להתחברות
    //מגדירים ראוטים, כל ראוטים הם ג'ייסון
    AngularFireModule.initializeApp(environment.firebase), //חיבור לDB
    AngularFireDatabaseModule,//גם חיבור
   AngularFireAuthModule,//גם חיבור
   MatSelectModule,
    RouterModule.forRoot([
      {path:'',component:SingupComponent}, //מכיל יו-אר-אל אם הוא ריק מדובר באינדקס בקומפוננט נרשום את הקומפוננטים שמשתנים
      {path:'welcome',component:WelcomeComponent},
      {path:'**',component:SingupComponent},
    
      ])
    
  ],
  
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
