import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { flush } from '@angular/core/testing';
import { AuthService } from '../auth.service';
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {
  @Input() data:any;
  @Output() myButtonClicked= new EventEmitter<any>();
  name:string;
  price:string;
  key:string;
  inStock:boolean;

  showDB1=false;//דיפוט- להעלים
  showDB2=false;//דיפוט- להעלים-כפתור ביטול
  showDB3=false;//דיפוט- להעלים-כפתור מחיקה סופית
  

  constructor(private authservice:AuthService, private db:AngularFireDatabase,) { }

  inStockChange(){
    this.authservice.user.subscribe(user=>{
      this.db.list('/user/'+user.uid+'/items').update(this.key, {'inStock':this.inStock})
     }
   )
 }



  showDeleteButton(){
    this.showDB1=true;//להראות
  }
  hideDeleteButton(){
    this.showDB1=false; //העלים
  }

  delete1(){//מחיקה ראשון
    this.showDB1=false;//שיעלם הכפתור
    this.showDB2=true;//שיעלם הכפתור
    this.showDB3=true;//שיעלם הכפתור
  }

  delete2(){ //כפתור ביטול
    this.showDB1=false;//שיעלם הכפתור
    this.showDB2=false;//שיעלם הכפתור
    this.showDB3=false;//שיעלם הכפתור
  }

  delete3(){//מחיקה סופית
    this.showDB1=false;//שיעלם הכפתור
    this.showDB2=false;//שיעלם הכפתור
    this.showDB3=false;//שיעלם הכפתור
    this.authservice.user.subscribe(user=>{
    this.db.list('/user/'+user.uid+'/items').remove(this.key)
    })
  }

  ngOnInit() {
this.key=this.data.$key
console.log(this.key);
this.name=this.data.name
this.price=this.data.price
this.inStock=this.data.inStock
  }

}
