import { Component, OnInit } from '@angular/core';
import {AuthService} from '../auth.service';//נחוץ כי רק באמצעותו אפשר להגיכ ליוזר ID
import {Router} from "@angular/router";

@Component({
  selector: 'gettings',
  templateUrl: './gettings.component.html',
  styleUrls: ['./gettings.component.css']
})
export class GettingsComponent implements OnInit {

  constructor(private router:Router, public authService:AuthService) { }

  ngOnInit() {
  }

}
