import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database'; //מוסיפים לדטהבייס

@Component({
  selector: 'list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  products=[];
  displayedColumns: string[] = ['name', 'color', 'price','currency'];

  constructor(private db:AngularFireDatabase,) { }
 
  ngOnInit() {
    this.db.list('/products').snapshotChanges().subscribe(
      products=>{
        this.products=[];
        products.forEach(
          product=> {
            let m= product.payload.toJSON();
            m["$key"] = product.key;
            this.products.push(m);
          }
        )
      }
    )
  }

}