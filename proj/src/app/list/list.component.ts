import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database'; //מוסיפים לדטהבייס
import {AuthService} from '../auth.service';//נחוץ כי רק באמצעותו אפשר להגיכ ליוזר ID
import { ListService } from '../list.service';// יבוא לסרביס
import {Router} from "@angular/router";
import {MatDialog, MatDialogConfig} from "@angular/material";
import { ProductComponent } from '../product/product.component';
import { EditComponent } from '../edit/edit.component';




@Component({
  selector: 'list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})


export class ListComponent implements OnInit {
  products=[];//להצגה
  displayedColumns: string[] = ['name', 'color', 'price','currency','edit','delete'];//להצגה
  currencies = [];////סינון
  name:string;// להוספה 
  color:string;//להוספה
  price:number;//להוספה
  currency=""; //להוספה
  key:string;


  public tempCurrency="all"; //דיפולט לסינון

  constructor(private db:AngularFireDatabase,
    private authService:AuthService,
    private listService:ListService,
    private router:Router,
    private dialog: MatDialog, ) {   }

  
  addproduct(name:string,color:string,price:number,currency:string){
  this.listService.addProduct(this.name,this.color,this.price,this.currency);

  this.name='';// מרוקן את התיבה
  this.color='';// מרוקן את התיבה
  this.price=null;// מרוקן את התיבה
  this.currency='';// מרוקן את התיבה
  }
  //------------------------------------------------------------------------------------
  isAuth(){
    return this.authService.isAuth()
  }
  //------------------------------------------------------------------------------------
  toDelete(key){
    this.listService.delete(key);
  }
  //------------------------------------------------------------------------------------

  toFilter()
  {
    this.authService.user.subscribe(user=>
      this.db.list('/user/'+user.uid+'/products').snapshotChanges().subscribe(
      products => {
        this.products = [];
        products.forEach(
          product => {
            let y = product.payload.toJSON();
            y["$key"] = product.key;
            if (this.tempCurrency == 'all') {     //כדי שלא יהיה כמו המספר שלמטה בדף
              this.products.push(y);
            }
            else if (y['currency'] == this.tempCurrency) { //כדי שלא יהיה כמו המספר שלמטה בדף
              this.products.push(y);
            }
          }
        )
      }
    )
  )
  }


  /**filter() {
    this.db.list('/products').snapshotChanges().subscribe(
     products => {
        this.products = [];
        products.forEach(
          movie => {
            let y = movie.payload.toJSON();
            if (this.products == 'all') {
              this.products.push(y);
            }
            else if (this.currency == y['currency'].toLowerCase()) {  
              this.movies.push(y);
            }
            else if (this.studio == '') {
              this.movies.push(y);
            }
          }
        )
      }
    )
  }*/
  //------------------------------------------------------------------------

  openDialog(element) {

    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    
    dialogConfig.data={
      name:element.name,
      key:element.$key,
      color:element.color,
      price:element.price,
      currency:element.currency
    }

    this.dialog.open(EditComponent, dialogConfig)
    this.tempCurrency='all';//כדי שהכל יוצג אחרי סינון
    this.toFilter(); //כדי שהכל יוצג אחרי סינון
    
      }
      //------------------------------------------------------------------------

  
  ngOnInit() {//לשם ההצגה
    if(this.isAuth()!=null)//רק אם יש יוזר מחובר
    {
    this.authService.user.subscribe(user => { //סובסקרייס מקבלת ארור פנקשן תמיד. מה שלפני החץ בפונקציה זה הקלט, מה שבתוך הסוגריים המסולסלים זה גוף הפונקציה
    this.db.list('/user/'+user.uid+'/products/').snapshotChanges().subscribe(
      products=>{
        this.products=[];
        this.currencies = ['all'];
        products.forEach(
          product=> {
            let p= product.payload.toJSON();
            p["$key"] = product.key;
            this.products.push(p);
         let cu = p['currency'];//סינון
         if (this.currencies.indexOf(cu)== -1) {//סינון
             this.currencies.push(cu);//סינון
            }
          }
        )
      }
    )
   })
  }
  else
  this.router.navigate(['home'])
  }


}