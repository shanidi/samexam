import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { flush } from '@angular/core/testing';
import { ListService } from '../list.service';





@Component({
  selector: 'product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

@Input() data:any;//דרוש לצורך בנית האלמננט
@Output() deleteButtonClicked = new EventEmitter<any>();// דיבור עם האבא


  name:string; //דרוש לצורך בנית האלמננט
  color:string;//דרוש לצורך בנית האלמננט
  currency:number;//דרוש לצורך בנית האלמננט
  price:string;//דרוש לצורך בנית האלמננט


  constructor(private listService:ListService){ }


//-----------------------------------------------------

//-----------------------------------------------------
  ngOnInit() {

    this.name = this.data.name;//דרוש לצורך בנית האלמננט
    this.color = this.data.color;//דרוש לצורך בנית האלמננט
    this.currency = this.data.currency;//דרוש לצורך בנית האלמננט
    this.price = this.data.price;//דרוש לצורך בנית האלמננט
    
  }

}